;;; flycheck-taskjuggler.el --- A Flycheck checker for Taskjuggler files  -*- lexical-binding: t; -*-

;; Copyright 2023 Alex Figl-Brick <alex@alexbrick.me>

;; Author: Alex Figl-Brick <alex@alexbrick.me>
;; Version: 0.1
;; Package-Requires: ((emacs "24.1") (flycheck "0.20"))
;; URL: https://gitlab.com/bricka/emacs-flycheck-taskjuggler

;; This program is free software: you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see
;; <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This package is a Flycheck syntax checker for Taskjuggler files.
;; It uses the --check-syntax flag to the tj3 command.
;;
;; For more information about TaskJuggler, see the homepage at
;; https://taskjuggler.org

;;; Code:

(require 'flycheck)

(flycheck-define-checker taskjuggler
  "A syntax checker for TaskJuggler files."
  :command ("tj3" "--check-syntax" source)
  :modes taskjuggler-mode
  :error-patterns
  ((error line-start (file-name) ":" line ": " "Error: " (message))))

(add-to-list 'flycheck-checkers 'taskjuggler)

(provide 'flycheck-taskjuggler)
;;; flycheck-taskjuggler.el ends here
